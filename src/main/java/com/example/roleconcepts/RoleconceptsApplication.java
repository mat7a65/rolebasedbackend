package com.example.roleconcepts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoleconceptsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoleconceptsApplication.class, args);



	}

}
