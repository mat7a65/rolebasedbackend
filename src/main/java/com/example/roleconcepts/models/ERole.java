package com.example.roleconcepts.models;

import org.assertj.core.util.Sets;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_CREATOR,
    ROLE_DECIDER;
}
