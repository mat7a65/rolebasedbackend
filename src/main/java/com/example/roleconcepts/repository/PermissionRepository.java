package com.example.roleconcepts.repository;

import com.example.roleconcepts.models.EPermission;
import com.example.roleconcepts.models.ERole;
import com.example.roleconcepts.models.Permission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PermissionRepository extends CrudRepository<Permission, Integer> {

    Optional<Permission> findByName(EPermission name);

}
