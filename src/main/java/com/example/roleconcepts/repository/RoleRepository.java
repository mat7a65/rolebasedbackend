package com.example.roleconcepts.repository;

import com.example.roleconcepts.models.ERole;
import com.example.roleconcepts.models.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {

    Optional<Role> findByName(ERole name);
}
